﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_09.Controllers
{
    public class AjaxController : Controller
    {
        // GET: Ajax
        public ActionResult Index()
        {
            return View();
        }

        public string CheckDetails(string param1, string param2)
        {
            var chk = new check
            {
                subject = "hello! " + param1,
                description = param2 + " Years Old"
            };
            return JsonConvert.SerializeObject(chk);
        }

        public class check
        {
            public string subject { get; set; }

            public string description { get; set; }
        }

        public ActionResult GetData()
        {
            var data = new { Name = "Kevin", Age = 40 };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Sample1()
        {
            return View();
        }
    } 
}